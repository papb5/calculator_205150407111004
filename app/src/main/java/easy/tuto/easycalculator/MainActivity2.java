package easy.tuto.easycalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import org.mozilla.javascript.tools.jsc.Main;

public class MainActivity2 extends AppCompatActivity {

    private TextView textView, textView2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        textView = findViewById(R.id.textView);
        textView2 = findViewById(R.id.textView2);

        Intent intent = getIntent();
        Bundle x = intent.getExtras();

        String jawab = x.getString(MainActivity.JAWAB);
        String perhitungan = x.getString(MainActivity.PERHITUNGAN);

        textView.setText(perhitungan);
        textView2.setText(jawab);

    }
}